import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const propTypes = {
  text: PropTypes.string,
  url: PropTypes.string,
  fillColor: PropTypes.string,
  borderColor: PropTypes.string,
  textColor: PropTypes.string
}

const Button = styled.a`
  background-color: ${props => props.fillColor};
  border: ${props => props.borderColor};
  color: ${props => props.textColor};
  min-width: 133px;
  padding: ${props => props.viewport === "mobile" ? "8px 15px" : "10px 20px"};
  text-align: center;
  font-size: ${props => props.viewport === "mobile" ? "10px" : "15px"};
  font-weight: 700;
  line-height: 2em;
  margin: 0px 15px;
  border-radius: 10px;
  transition: color .5s ease,background-color .5s ease;

  &:hover{ 
    background-color: ${props => props.textColor};
    color: ${props => props.fillColor};
    transition: color .5s ease,background-color .5s ease;
    text-decoration: none;
  }
`;

const NavButton = (props) => {

  const { viewport, text, url, fillColor, borderColor, textColor } = props;
  return (
    <Button
      viewport={viewport}
      fillColor={fillColor}
      borderColor={borderColor}
      textColor={textColor}
      href={url}
    >
      { text }
    </Button>
  );

}

export default NavButton;