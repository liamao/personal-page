import React from 'react';
import styled from 'styled-components';
import NavButton from "./NavButton";
import navlogo from "../../../assets/navlogo.png";

const Header = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  padding: 20px;
`;

const LogoWrapper = styled.div`
  height: 50px;
  width: 64px;
  box-sizing: border-box;
`;

const ButtonsWrapper = styled.div`
  margin-top: 10px;
  margin-left: auto;
`
const NavigationBar = (props) => {

  const { viewport } = props;

  return (
    <Header>
      <ButtonsWrapper>
        <NavButton 
          viewport={viewport}
          text="MY WORK"
          url="/"
          fillColor="black"
          borderColor="solid #00b2fc"
          textColor="#00b2fc"
        />
        <NavButton 
          viewport={viewport}
          text="CONTACT"
          url="/"
          fillColor="white"
          borderColor="solid white"
          textColor="black"
        />
      </ButtonsWrapper>
    </Header>

  );
}

export default NavigationBar;
