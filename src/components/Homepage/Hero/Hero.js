import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import anime from "animejs";

const propTypes = {
  viewport: PropTypes.string,
}

const titlePhrases = [
  "program",
  "engineer",
  "design"
];

const HeroWrapper = styled.div`
  height: ${props => props.height};
  width: 100%;
  padding: ${({ viewport }) =>
    viewport === "mobile" ? "100px 40px" : "100px 40px"};
`;

const Subtitle = styled.h6`
  color: white;
  font-family: 'Roboto', sans-serif;
  font-weight: 400;
  font-size: calc(27px + 10*(100vw - 375px)/1065);
  text-indent: ${({ viewport }) =>
    viewport === "mobile" ? "0px" : viewport === "tablet" ? "0px" : "6px"};
`;

const TitleWrapper = styled.div`
  position: relative;
  overflow: hidden;
`;

const Title = styled.h6`
  font-family: 'Roboto', sans-serif;
  font-weight: 700;
  font-size: calc(27px + 154*(100vw - 375px)/1065);
  color: white;
  line-height: 100%;
  display:inline;
  float:left;
`;

const TitleBounce = styled.h6`
  position: absolute;
  font-family: 'Roboto', sans-serif;
  font-weight: 700;
  font-size: calc(27px + 154*(100vw - 375px)/1065);
  color: white;
  line-height: 95%;
  display:inline;
  margin-left: ${({ viewport }) => viewport === "mobile" ? "10px" : "40px"};
`;

const Letter = styled.span`
  display:inline-block;
  float: left;
`;

const getHeroHeight = ({viewport}) => {
  switch(viewport){
    case "mobile":
      return "285px";
    case "tablet":
      return "400px";
    default:
      return "525px";
  }
}

const getNextPhrase = (index) => {
  if (!titlePhrases[index+1]){
    return 0;
  }
  return index+1;
}

class Hero extends React.Component {
  constructor(props) {
    super(props);
    window.heroTop = this;
    this.state = {
      index: 0
    }
  }

  animate(hero){
    anime.timeline()
    .add({
        targets: '.letter',
        translateY: [150,0],
        translateZ: 0,
        opacity: [0,1],
        easing: "easeOutExpo",
        duration: 800,
        delay: (el, i) => 300 + 30 * i
      }).add({
        targets: '.letter',
        translateY: [0,-150],
        opacity: [1,0],
        easing: "easeInExpo",
        duration: 400,
        delay: (el, i) => 100 + 30 * i,
        complete: function(anim) {
          hero.increaseIndex();

          hero.animate(hero);
        }
      });
    }

  increaseIndex(){
    this.setState((state) => {
      if (state.index === titlePhrases.length - 1){
        return {index: 0}
      }
      return {index: state.index + 1}
    });
  }


  componentDidMount(){
    this.animate(this);
  }

  render(){

  const { viewport } = this.props;
  const { index } =  this.state;

  const phrase = titlePhrases[index].split('');
  const height = getHeroHeight(viewport);

    return (
      <HeroWrapper
        viewport={viewport}
        height={height}
      >
        <Subtitle viewport={viewport}>Hi, I'm Liam Obrochta.</Subtitle>
        <TitleWrapper>
          <Title viewport={viewport}>I</Title>
          <TitleBounce viewport={viewport}>
            {
              phrase.map(letter =>{
                return <Letter className="letter">{letter}</Letter>
              })
            }
          </TitleBounce>
        </TitleWrapper>

      </HeroWrapper>
    );
  }
}


export default Hero;