import React from 'react';
import { 
  Route,
  Switch,
  Redirect 
} from "react-router-dom";
import { pxToInt } from './utils/pxToInt'
import enquire from 'enquire.js';
import breakpoints from "./breakpoints";
import Homepage from './pages/Homepage';
import Stats from './pages/Stats';

let desktop = { min: pxToInt(breakpoints.lg), max: null };
let tablet = {
  max: pxToInt(breakpoints.lg),
  min: pxToInt(breakpoints.sm),
};
let mobile = { max: pxToInt(breakpoints.sm), min: null };

const desktopQuery = `screen and (min-width:${breakpoints.lg})`;
const tabletQuery = `screen and (max-width:${breakpoints.lg})`;
const mobileQuery = `screen and (max-width:${breakpoints.md})`;


class App extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
       viewport: this._calculateInitialViewport()
    }
  }

  componentDidMount() {
    let self = this;
    //If it goes from Desktop to Tablet
    enquire.register(tabletQuery, {
        match : function() {
          self._changeViewport('tablet');
        }
    });

    //If it goes from Tablet to Desktop    
    enquire.register(desktopQuery, {
        match : function() {
          self._changeViewport('desktop');
        }
    });
    //If it goes from Tablet to Mobile    
    enquire.register(mobileQuery, {
        match : function() {
          self._changeViewport('mobile');
        },
        unmatch : function() {
          self._changeViewport('tablet');
        }
    });
  }

  _calculateInitialViewport = () => {
    const width = Math.max(
      document.documentElement.clientWidth,
      window.innerWidth || 0
    );
    let viewport;
    if (
      (desktop.min === null || (desktop.min && width >= desktop.min)) &&
      (desktop.max === null || (desktop.max && width <= desktop.max)) &&
      (desktop.min !== null || desktop.max !== null)
    ) {
      viewport = 'desktop';
    } else if (
      (tablet.min === null || (tablet.min && width >= tablet.min)) &&
      (tablet.max === null || (tablet.max && width <= tablet.max)) &&
      (tablet.min !== null || tablet.max !== null)
    ) {
      viewport = 'tablet';
    } else if (
      (mobile.min === null || (mobile.min && width >= mobile.min)) &&
      (mobile.max === null || (mobile.max && width <= mobile.max)) &&
      (mobile.min !== null || mobile.max !== null)
    ) {
      viewport = 'mobile';
    }
    return viewport;
  }

  _changeViewport = (viewport) => {
    this.setState({viewport: viewport});
  }


  _renderHomepage = (props) => {
    return (<Homepage viewport={this.state.viewport}/>);
  }

  _renderStats = (props) => {
    return (<Stats viewport={this.state.viewport}/>);
  }

  render() {

    return (
       <Switch>
          <Route path="/" component={this._renderHomepage}/>
          <Route path="/stats" component={this._renderStats}/>
          <Redirect to="/"/>
       </Switch>
    );

  }
}

export default App;
