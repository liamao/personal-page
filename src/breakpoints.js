export default {
  xs: '320px',
  sm: '544px',
  md: '768px',
  lg: '991px',
  xl: '1272px',
};
