import React from 'react';
import styled from 'styled-components';
import NavigationBar from "../components/Homepage/Navbar/NavigationBar";
import Hero from "../components/Homepage/Hero/Hero";
import HeroImage from "../components/Homepage/Hero/Hero";

const Wrapper = styled.div`
  height: 100vh;
  background-color: black;
  display: flex;
  flex-direction: column;
`;

const NavContainer = styled.div`
`;
const BodyContainer = styled.div`
  margin-top: 45px;
`

const Homepage = (props) => {

  const { viewport } = props;

  return (
    <Wrapper>
      <NavContainer>
        <NavigationBar viewport={viewport}/>
      </NavContainer>
      <BodyContainer>
        <Hero viewport={viewport}/> 
      </BodyContainer>
    </Wrapper>
  );
}

export default Homepage;

